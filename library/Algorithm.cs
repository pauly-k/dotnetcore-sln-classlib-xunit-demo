﻿using System;

namespace library
{
    public class Algorithm
    {
        public int Fibonacci(int n) {
            if (n == 0) return 0;
            if (n == 1) return 1;

            var a = 0;
            var b = 1;
            for (var i = 2; i <= n; i++) {
                var tmp = b;
                b = a + b;
                a = tmp;
            }

            return b;
        }
    }
}
