# Demo .netcore solution with classlib and xunit tests

### new class lib test sln

Base on
[Create a .NET Core solution in macOS using Visual Studio Code](https://docs.microsoft.com/th-th/dotnet/core/tutorials/using-on-Smacos)

## setup

```bash
dotnet new sln -o hacker-rank
cd hacker-rank
dotnet new classlib -o library
dotnet new xunit -o tests
dotnet sln add library/library.csproj
dotnet sln add tests/tests.csproj
cd tests
dotnet add reference ../library/library.csproj
```

## test watcher

Add the following to the tests.csproj then run `dotnet restore`

```xml
<ItemGroup>
 <DotNetCliToolReference Include="Microsoft.DotNet.Watcher.Tools" Version="1.0.0" />
</ItemGroup>
```
