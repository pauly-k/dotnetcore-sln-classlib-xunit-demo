using System;
using Xunit;
using library;
using System.Linq;

namespace tests
{
	public class AlgorithmTests
	{
        private readonly Algorithm _alg;

        public AlgorithmTests() {
            _alg = new Algorithm();
        }

		[Fact]
		public void FibonacciTest()
		{
			var n = 9;
			var expected_result = 34;
			var actual = _alg.Fibonacci(n);
			Assert.Equal(expected_result, actual);
		}

		[Fact]
		public void FibonacciTest_2()
		{
			var tests = new[] {
                new { n = 3, r = 2 },
                new { n = 4, r = 3 },
                new { n = 5, r = 5 },
                new { n = 6, r = 8 },
                new { n = 7, r = 13 },
                new { n = 8, r = 21 },
                new { n = 9, r = 34 }
            };


			tests.ToList().ForEach(test =>
			{
				var result = _alg.Fibonacci(test.n);
                Assert.Equal(test.r, result);
			});

		}
	}
}
